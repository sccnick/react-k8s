FROM node:8.16.0-alpine AS build-stage

WORKDIR /app

COPY package*.json yarn.lock ./

# Set Proxy
ENV HTTP_PROXY http://10.137.18.77:3128

# Install app dependencies
RUN yarn config set proxy $HTTP_PROXY \
    && yarn

COPY . .

RUN yarn build

FROM nginx:1.15.12-alpine

ARG CONTAINER_PORT

COPY --from=build-stage /app/build/ /usr/share/nginx/html

EXPOSE ${CONTAINER_PORT}

CMD ["nginx", "-g", "daemon off;"]